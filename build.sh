#! /bin/bash

here="$(realpath "$(dirname "${0}")")"


mainFunction () {
	buildBinary
	buildMan
}


buildBinary () {
	so mkdir --parents "${here}/_ROOT/usr/bin/"
	so mkdir --parents "${here}/_BUILD"
	so cp "${here}/assets"/*.go "${here}/_BUILD/"

	export GOPATH="${here}/_BUILD/gopath"
	cd "${here}/_BUILD"

	so go mod init "blobs.go"
	so go mod tidy
	so go fmt "${here}/_BUILD/blobs.go"
	so go build -o "${here}/_ROOT/usr/bin/" "${here}/_BUILD/blobs.go"

	removeDir "${here}/_BUILD"
}


buildMan () {
	mkdir --parents "${here}/_ROOT/usr/share/man/man1"
	cp "${here}/assets/blobs.1" "${here}/_ROOT/usr/share/man/man1/"
}


cleanUp () {
	removeDir "${here}/_BUILD"
	removeDir "${here}/_ROOT"
}


prepareEnvironment () {
	set -eu
	cleanUp
	cd "${here}"
}


removeDir () {
	local dir="${1}"

	if [[ -d "${dir}" ]]; then
		so find "${dir}" -exec chmod +w {} \;
		so rm --recursive --force "${dir}"
	fi
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


prepareEnvironment
mainFunction
