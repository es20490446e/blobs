package main

import (
	"bytes"
	"fmt"
	"github.com/zenthangplus/goccm"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
)

func main() {
	setDir()
	semaphore := goccm.New(runtime.NumCPU())
	
	filepath.Walk(".", func(file string, info os.FileInfo, err error) error {
		semaphore.Wait()
	
		go func() {
    		if isRunable(file) {
				fmt.Println(file)
			}
			
			semaphore.Done()
        }()
		
		return nil
	})
	
	semaphore.WaitAllDone()
}

func isRunable(file string) bool {
	mime := mime(file)
	mimetype := mime[0]
	charset := mime[1]

	if bytes.Contains(mimetype, []byte("application/x-")) && bytes.Contains(charset, []byte("binary")) {
		return true
	}

	return false
}

func mime(file string) [][]byte {
	cmd := exec.Command("file", "--mime", "--brief", file)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "mime: %s: %v\n", file, err.Error())
		os.Exit(1)
	}

	split := bytes.Split(stdout, []byte(";"))
	mimetype := split[0]

	split = bytes.Split(stdout, []byte("="))
	charset := split[1]

	return [][]byte{mimetype, charset}
}

func setDir() {
	dir := "."

	if len(os.Args) > 2 {
		fmt.Fprintf(os.Stderr, "Surplus arguments in: %s\n", os.Args[1:])
		fmt.Println("Surround dir with quotes")
		os.Exit(1)
	} else if len(os.Args) > 1 {
		dir = os.Args[1]
	}
	
	err := os.Chdir(dir)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
