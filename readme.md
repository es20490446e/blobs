```
USAGE
       blobs [dir]

PIPING
       cd "${dir}"; blobs | xargs -rd '\n' [command] [--force]

IGNORED
       Unreadable dirs

       Archive contents
